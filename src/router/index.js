import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";
Vue.use(VueRouter);

const routes = [{
        path: "/",
        alias: "/landing",
        name: "landing",

        component: () =>
            import ("../views/Landing.vue"),
    },
    {
        path: "/profile",
        name: "profile",
        meta: { requiresAuth: true },

        component: () =>
            import ("../views/Profile.vue"),
    },
    {
        path: "/cart",
        name: "cart",
        meta: { requiresAuth: true },
        component: () =>
            import ("../views/Cart.vue"),
    },
    {
        path: "/history",
        name: "history",
        meta: { requiresAuth: true },

        component: () =>
            import ("../views/History.vue"),
    },
    {
        path: "/Store",
        name: "Store",

        component: () =>
            import ("../views/Store.vue"),
    },
    {
        path: "/login",
        alias: ["/register", "/login"],
        name: "Auth",
        component: () =>
            import ("../views/Auth.vue"),
    },

    {
        path: "/*",
        name: "Error",
        component: () =>
            import ("../views/Error.vue"),
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (store.getters.getUser.name == "") {
            next({ path: "/login" });
        } else {
            next();
        }
    } else {
        next();
    }
});
export default router;