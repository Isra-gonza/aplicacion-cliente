import Vue from "vue";
import Vuex from "vuex";
import router from "../router/index";
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        logguedUser: {
            created_at: "",
            email: "",
            id: "",
            name: "",
            rol: "",
            updated_at: "",
        },
        ordersHistory: [],

        products: [],
        cart: [],
        modal: {
            active: false,
            type: "",
            text: "",
        },
    },
    mutations: {
        setloguedUser(state, user) {
            try {
                state.logguedUser = user;
                return { success: true, msg: "usuario almacenado ", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },

        setProducts(state, products) {
            try {
                Vue.set(state, "products", products);
                return { success: true, msg: "ordenes almacenadas ", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 6 };
            }
        },
        addProductToCart(state, product) {
            try {
                product.qty = 1;
                state.cart.push(product);
                return {
                    success: true,
                    msg: "producto añadido correctamente ",
                    errorCode: 0,
                };
            } catch (error) {
                return {
                    success: false,
                    msg: "error añadiendo el producto ",
                    errorCode: 7,
                };
            }
        },
        setOrdersHistory(state, order) {
            try {
                state.ordersHistory = [];
                state.ordersHistory.push(...order);
                return {
                    success: true,
                    msg: "order añadida correctamente al historial ",
                    errorCode: 0,
                };
            } catch (error) {
                return {
                    success: false,
                    msg: "error añadiendo la orden ",
                    errorCode: 8,
                };
            }
        },
        removeProductFromCart(state, productId) {
            try {
                state.cart.splice(
                    state.cart.findIndex((product) => product.id == productId),
                    1
                );

                return {
                    success: true,
                    msg: "producto añadido correctamente ",
                    errorCode: 0,
                };
            } catch (error) {
                return {
                    success: false,
                    msg: "error ñadiendo el producto ",
                    errorCode: 7,
                };
            }
        },
        setModal(state, modal) {
            try {
                state.modal = modal;
                setTimeout(() => {
                    state.modal = {};
                }, 5000);
                return { success: true, msg: "modal activado", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error activando modal", errorCode: 7 };
            }
        },
    },
    getters: {
        getUser(state) {
            return state.logguedUser;
        },
        getProducts(state) {
            return state.products;
        },
        getCartProducts(state) {
            return state.cart;
        },
        getOrdersHistory(state) {
            return state.ordersHistory;
        },
        getModal(state) {
            return state.modal;
        },
    },
    actions: {
        async getLoggedUser(context) {
            try {
                let response = await Vue.axios.get("/api/user");
                console.log("asd");
                if (response.data.rol == "customer") {
                    context.commit("setloguedUser", response.data);

                    return {
                        success: true,
                        msg: "guardados datos de logueo",
                        errorCode: 0,
                    };
                }

                return {
                    success: false,
                    msg: "error al guardar datos de logueo",
                    errorCode: 4,
                };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async removeOrder(context, orderId) {
            try {
                let response = await Vue.axios.delete("/api/ordersapi/" + orderId);
                if (!response.data.success) {
                    return {
                        success: false,
                        msg: "error al elimminar la ordren",
                        errorCode: 4,
                    };
                }

                context.dispatch("getOrdersHistory");
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async getProducts(context) {
            try {
                let response = await Vue.axios.get("/api/productsapiall");
                if (!response.data) {
                    return {
                        success: false,
                        msg: "error traer los productos ",
                        errorCode: 4,
                    };
                }

                context.commit("setProducts", response.data);
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async getOrdersHistory(context) {
            try {
                let response = await Vue.axios.get(
                    "/api/filterorder/" + context.state.logguedUser.id
                );
                if (!response.data) {
                    return {
                        success: false,
                        msg: "error traer las ordenes ",
                        errorCode: 4,
                    };
                }

                context.commit("setOrdersHistory", response.data);
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async doAComanda(context, address) {
            try {
                let comanda = {
                    state: 0,
                    address: address,

                    user_id: context.state.logguedUser.id,
                };
                let OrderResponse = await Vue.axios.post("/api/ordersapi", comanda);
                if (!OrderResponse.data) {
                    return {
                        success: false,
                        msg: "error traer los productos ",
                        errorCode: 4,
                    };
                }
                context.state.cart.forEach(async(product) => {
                    try {
                        let order = {
                            quantity: product.qty,
                            price: product.price,
                            discount: 0,
                            order_id: OrderResponse.data.id,
                            product_id: product.id,
                        };

                        await Vue.axios.post("/api/orderlinesapi", order);
                    } catch (error) {
                        return { success: false, msg: "error lineapi", errorCode: 15 };
                    }
                    return { success: true, msg: "lineas agregadas", errorCode: 0 };
                });
            } catch (error) {
                console.log(error);
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
        async login(context, userCredential) {
            try {
                let response = await Vue.axios.post("/api/loginapi", userCredential);
                if (!response.data.token)
                    return { success: false, msg: "error al loguearse", errorCode: 3 };
                Vue.$cookies.set("Authorization", `Bearer ${response.data.token}`);
                let user = await context.dispatch("getLoggedUser");
                console.log(user);
                if (user.success) {
                    router.push("/profile");
                    return { success: false, msg: "error al loguearse", errorCode: 3 };
                } else {
                    return { success: false, msg: "error al loguearse", errorCode: 3 };
                }
            } catch (error) {
                return { success: false, msg: "error al loguearse", errorCode: 3 };
            }
        },

        async register(context, userCredential) {
            try {
                userCredential.rol = "customer";
                let response = await Vue.axios.post("/api/registerapi", userCredential);
                if (!response.data.name)
                    return { success: false, msg: "error al registrarse", errorCode: 2 };
                const user = {
                    email: userCredential.email,
                    password: userCredential.password,
                };
                context.dispatch("login", user);
                return { success: true, msg: "registrado correctamente", errorCode: 0 };
            } catch (error) {
                return { success: false, msg: "error ", errorCode: 5 };
            }
        },
    },
    modules: {},
});