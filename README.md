<a name="item0"></a>
# APLICACIÓN CLIENTE

USUARIO PRUEBA: customer@customer.customer, contraseña: customer.

## ÍNDICE DE CONTENIDOS

* [APLICACIÓN CLIENTE](#item0)   
* [CREDENCIALES PARA CONECTARSE](#item1)   
* [PUESTA EN MARCHA](#item2) 
* [URL DEL SITIO WEB](#item3)   
* [PUERTOS ENTRADA Y SALIDA](#item4) 
* [RESULTADO FINAL](#item5) 



<a name="item1"></a>
## CREDENCIALES PARA CONECTARSE
**IP:** 52.3.253.121  
**Puerto:** 443   
**Usuarios disponibles(Con permisos):** ubuntu(sin contraseña), client(1234),

<a name="item2"></a>
## PUESTA EN MARCHA
Instalación previa node JS
Instalación previa de GIT
Instalación previa de vue-cli-service y vue 2.x.  

**FUNCIONAMIENTO EN LOCAL:**   

```sh
git clone https://gitlab.com/Isra-gonza/aplicacion-cliente.git   

npm install   

npm run serve
```
**PRODUCCIÓN:**

Tan solo tenemos que ejcutar el script.sh situado en la base del proyecto

Comando para ejecutar el archivo script.sh:

```sh
sh script.sh
```

<a name="item3"></a>
## URL DEL SITIO WEB   
[Enlace a la página de los clientes][url]
<a name="item4"></a>
## PUERTOS ENTRADA Y SALIDA


![](https://media.discordapp.net/attachments/798220960473808921/812454522839498762/unknown.png?width=1389&height=181)
![](https://media.discordapp.net/attachments/808372312986222645/812296583348617236/unknown.png?width=1440&height=421) 

<a name="item5"></a>
## RESULTADO FINAL

**LANDING:**   
![](https://media.discordapp.net/attachments/808372312986222645/812671013762498590/unknown.png?width=753&height=660)

**PRODUCTOS:**

![](https://media.discordapp.net/attachments/808372312986222645/812671351651172372/unknown.png?width=1440&height=353)

**ÓRDENES DEL CLIENTE:**

![](https://media.discordapp.net/attachments/808372312986222645/812671721781592074/unknown.png?width=1440&height=551)

**CARRITO:**

![](https://media.discordapp.net/attachments/808372312986222645/812671988241399848/unknown.png?width=1440&height=539)

[VOLVER](#item0) 

[url]: <http://www.g05.batoilogic.es/>
